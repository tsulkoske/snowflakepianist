﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System;

using Random = System.Random;


public class GenerativePiano_SmartNoteSwitcher : MonoBehaviour 
{
	//Octave Booleans, to monitor which octave of a chord is active
	public bool _lowOctave = false;
	public bool _midOctave = false;
	public bool _hiOctave = false;

	//Key Booleans, to monitor which key signature the piano is playing in
	public bool isAMajor, isAbMajor, isBMajor, isBbMajor, isCMajor, isDbMajor, isDMajor, isEbMajor, isEMajor, isFMajor, isFSharpMajor, isGMajor;

	//Chord Booleans, to monitor which Chord is being played
	public bool IChord, iiChord, iiiChord, IVChord, VChord, viChord, viAd2Chord;

	//Wwise Callback Boolean, to see if Wwise is sending Callback Strings from the MIDI files
	private bool WwiseCallback = false;

	//Detects whether there is a change in the Switch booleans
	//and is used by other snowflake scripts to re-evaluate the bools.
	public bool isSwitcherActivated = false;
	public bool isSwitchCompleted = false;

	//When WwiseCallback == true, RandomOctaveSwitcher() is called in Update(), 
	//then according to the above bools, TenSidedDie is rolled and will decide if 
	//the piano switches to the next closest octave.
	Random randomOctaveSwitchPercent = new Random ();
	int TenSidedDie;

	//Random ints used to select the starting Wwise Event
	Random randomSelectKey = new Random ();
	Random randomSelectChord = new Random ();
	Random randomSelectOctave = new Random ();
	int keyInt;
	int chordInt;
	int octaveInt;

	//The number of collisions (notes played) before Wwise switches to another Chord within the Key
	Random random_NumberOfNotesInChord = new Random ();
	int numberOfNotesInt;
	int collisions_SinceLastChordSwitch;

	//Assigns a percentage that determines how likely it is that a given chord will play next
	Random random_ChordSwitchPercent = new Random ();
	int OneHundredSidedDie;


	void Start () 
	{
		numberOfNotesInt = random_NumberOfNotesInChord.Next (4, 10);
		collisions_SinceLastChordSwitch = 0;

		OneHundredSidedDie = 0;

		keyInt = randomSelectKey.Next (1, 12);
		chordInt = randomSelectChord.Next (1, 7);
		octaveInt = randomSelectOctave.Next (1, 3);

		switch (keyInt)
		{
			case 1: isAMajor = true; break;
			case 2: isAbMajor = true; break;
			case 3: isBMajor = true; break;
			case 4: isBbMajor = true; break;
			case 5: isCMajor = true; break;
			case 6: isDbMajor = true; break;
			case 7: isDMajor = true; break;
			case 8: isEbMajor = true; break;
			case 9: isEMajor = true; break;
			case 10: isFMajor = true; break;
			case 11: isFSharpMajor = true; break;
			case 12: isGMajor = true; break;
					
		}

		switch (chordInt)
		{
			case 1: IChord = true; break;
			case 2: iiChord = true; break;
			case 3: iiiChord = true; break;
			case 4: IVChord = true; break;
			case 5: VChord = true; break;
			case 6: viChord = true; break;
			case 7: viAd2Chord = true; break;
		}

		switch (octaveInt) 
		{
			case 1: _lowOctave = true; break;
			case 2: _midOctave = true; break;
			case 3: _hiOctave = true; break;
		}


		ChordSelector ();
	
	}


//	void OnParticleCollision (GameObject other)
//	{
//
//		HandModel handModel = null;
//		if (other.transform.parent) 
//		{
//			handModel = other.transform.parent.GetComponent<HandModel> ();
//
//			if (handModel == null) 
//			{
//				handModel = other.transform.parent.parent.GetComponent<HandModel> ();
//			}
//		}
//
//		if (handModel == null)
//			return;
//
//		collisions_SinceLastChordSwitch += 1;
//		Debug.Log (collisions_SinceLastChordSwitch + "total collisions.");
//
//		AkSoundEngine.PostEvent ("Play_KeysAndModes", gameObject);
//		Debug.Log ("Triggered!");
//	}



	void Update () 
	{
		if (collisions_SinceLastChordSwitch >= numberOfNotesInt) 
		{
			ChordSwitcher ();
			isSwitcherActivated = true;
			OneHundredSidedDie = 0;
			collisions_SinceLastChordSwitch = 0;
			numberOfNotesInt = random_NumberOfNotesInChord.Next (4, 10);

			if (isSwitchCompleted == true) 
			{
				isSwitcherActivated = false;
			}
		}
	}




	//
	//
    //                !!*!!~-------------- CUSTOM FUNCTIONS: --------------~!!*!!
	//
	// 
	// 
	//
	// 

	//                  1. OCTAVE SWITCHER


//	Smartly switches between octaves when playing through notes in a chord
	void RandomOctaveSwitcher(string currentNoteOctave)
	{
		//LOW OCTAVE SWITCHER
		if (_lowOctave == true) 
		{
			//Reads the string sent from Wwise, and determines which Probability is triggered
			if (currentNoteOctave.Contains ("SwitchUpToMidHighProbability")) 
			{
				//Roll the die, and if the value is on or between 1 and 8, switch the octave,
				//and trigger the chord selector.  
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 8) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (currentNoteOctave.Contains ("SwitchUpToMidMediumProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 5) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (currentNoteOctave.Contains ("SwitchUpToMidLowProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 3) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					_lowOctave = true;
					return;
			}
		} 
	
		//MIDDLE OCTAVE SWITCHER
		else if (_midOctave == true) 
		{
			//Switch from Mid octave to High octave
			if (currentNoteOctave.Contains ("SwitchUpToHiHighProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 8) {
					_midOctave = false;
					_lowOctave = false;
					_hiOctave = true;
					ChordSelector ();
				} else
					return;
			}
			else if (currentNoteOctave.Contains ("SwitchUpToHiMediumProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 5) {
					_midOctave = false;
					_lowOctave = false;
					_hiOctave = true;
					ChordSelector ();
				} else
					return;
			}
			else if (currentNoteOctave.Contains ("SwitchUpToHiLowProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 3) {
					_midOctave = false;
					_lowOctave = false;
					_hiOctave = true;
					ChordSelector ();
				} else
					return;
			}

			//Switch from Mid octave to Low octave
			else if (currentNoteOctave.Contains ("SwitchDownToLowHighProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 8) {
					_midOctave = false;
					_lowOctave = true;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (currentNoteOctave.Contains ("SwitchDownToLowMediumProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 5) {
					_midOctave = false;
					_lowOctave = true;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (currentNoteOctave.Contains ("SwitchDownToLowLowProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 3) {
					_midOctave = false;
					_lowOctave = true;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}


		} 

		//HIGH OCTAVE SWITCHER
		else if (_hiOctave == true) 
		{

			if (currentNoteOctave.Contains ("SwitchDownToMidHighProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 8) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (currentNoteOctave.Contains ("SwitchDownToMidMediumProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 5) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (currentNoteOctave.Contains ("SwitchDownToMidLowProbability")) 
			{
				TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
				if (TenSidedDie <= 3) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////



	//         2. CHORD SWITCHER



	//After a given number of collisions (notes played), ChordSwitcher() is called
	//to smartly alter the chord booleans and choose the next chord.
	void ChordSwitcher()
	{
		if (OneHundredSidedDie == 0) 
		{
			OneHundredSidedDie = random_ChordSwitchPercent.Next (1, 100);
		}

		//Switching from the I Chord
		if (IChord == true) 
		{
			//30% chance to switch to V
			if (OneHundredSidedDie <= 30)
			{
				VChord = true;
				IChord = false;
				ChordSelector();
			}

			//24% chance to switch to IV
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				IVChord = true;
				IChord = false;
				ChordSelector ();
			}

			//13% chance to switch to iii
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				iiiChord = true;
				IChord = false;
				ChordSelector();
			}

			//19% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 70 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 70 && OneHundredSidedDie <= 79) 
				{
					viChord = true;
					IChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					IChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to ii
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				iiChord = true;
				IChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the ii Chord
		else if (iiChord == true) 
		{
			//30% chance to switch to V
			if (OneHundredSidedDie <= 30)
			{
				VChord = true;
				iiChord = false;
				ChordSelector();
			}

			//24% chance to switch to iii
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				iiiChord = true;
				iiChord = false;
				ChordSelector ();
			}

			//13% chance to switch to I
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				IChord = true;
				iiChord = false;
				ChordSelector();
			}

			//19% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 70 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 70 && OneHundredSidedDie <= 79) 
				{
					viChord = true;
					iiChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					iiChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to IV
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				IVChord = true;
				iiChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the iii Chord
		else if (iiiChord == true) 
		{
			//30% chance to switch to IV
			if (OneHundredSidedDie <= 30)
			{
				IVChord = true;
				iiiChord = false;
				ChordSelector();
			}

			//24% chance to switch to V
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				VChord = true;
				iiiChord = false;
				ChordSelector ();
			}

			//13% chance to switch to ii
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				iiChord = true;
				iiiChord = false;
				ChordSelector();
			}

			//24% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 77) 
				{
					viChord = true;
					iiiChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					iiiChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to I
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				IChord = true;
				iiiChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the IV Chord
		else if (IVChord == true) 
		{
			//30% chance to switch to I
			if (OneHundredSidedDie <= 30)
			{
				IChord = true;
				IVChord = false;
				ChordSelector();
			}

			//24% chance to switch to V
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				VChord = true;
				IVChord = false;
				ChordSelector ();
			}

			//13% chance to switch to ii
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				iiChord = true;
				IVChord = false;
				ChordSelector();
			}

			//24% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 77) 
				{
					viChord = true;
					IVChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					IVChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to iii
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				iiiChord = true;
				IVChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the V Chord
		else if (VChord == true) 
		{
			//40% chance to switch to I
			if (OneHundredSidedDie <= 40)
			{
				IChord = true;
				VChord = false;
				ChordSelector();
			}

			//23% chance to switch to IV
			else if (OneHundredSidedDie >= 41 && OneHundredSidedDie <= 64)
			{
				IVChord = true;
				VChord = false;
				ChordSelector ();
			}

			//4% chance to switch to ii
			else if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 69)
			{
				iiChord = true;
				VChord = false;
				ChordSelector();
			}

			//39% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 60 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 77) 
				{
					viChord = true;
					VChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					VChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to iii
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				iiiChord = true;
				VChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the vi Chord
		else if (viChord == true || viAd2Chord == true) 
		{
			//30% chance to switch to V
			if (OneHundredSidedDie <= 30)
			{
				VChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector();
			}

			//24% chance to switch to IV
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				IVChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector ();
			}

			//13% chance to switch to ii
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				iiChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector();
			}

			//24% chance to switch to I
			else if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 89) 
			{
				IChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector ();
			}

			//10% chance to switch to iii
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				iiiChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector ();
			}
		} 
		else
			return;

	}

	////////////////////////////////////////////////////////////////////////////////////////


	//       3. CHORD SELECTOR


	//Decides which Wwise Event is called, 
	//which in turn decides which collection of notes to play.
	void ChordSelector()
	{
		if (isAMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isAMajor IChord, No Music Selected!");
				}
			}
		} else if (isAbMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isAbMajor IChord, No Music Selected!");
				}
			}
		} else if (isBMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isBMajor IChord, No Music Selected!");
				}
			}
		} else if (isBbMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isBbMajor IChord, No Music Selected!");
				}
			}
		} else if (isCMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isCMajor IChord, No Music Selected!");
				}
			}
		} else if (isDbMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isDbMajor IChord, No Music Selected!");
				}
			}
		} else if (isDMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isDMajor IChord, No Music Selected!");
				}
			}
		} else if (isEbMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isEbMajor IChord, No Music Selected!");
				}
			}
		} else if (isEMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isEMajor IChord, No Music Selected!");
				}
			}
		} else if (isFMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isFMajor IChord, No Music Selected!");
				}
			}
		} else if (isFSharpMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isFSharpMajor IChord, No Music Selected!");
				}
			}
		} else if (isGMajor == true) {
			if (IChord == true) {
				if (_lowOctave == true) {

				} else if (_midOctave == true) {

				} else if (_hiOctave == true) {
				} else {
					Debug.Log ("isGMajor, No Music Selected!");
				}
			}
		} 	
	}
}
