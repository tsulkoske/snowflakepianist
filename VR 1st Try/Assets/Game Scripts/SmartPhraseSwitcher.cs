﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System;

using Random = System.Random;

public class SmartPhraseSwitcher : MonoBehaviour 
{
	public GameObject SingleNoteSystem;
	public SmartNoteSwitcher switchScript;

	public bool isPhraseSwitchCompleted = false;

	void Start () 
	{
		SingleNoteSystem = GameObject.FindGameObjectWithTag ("Single_Note_System");
		switchScript = SingleNoteSystem.GetComponent<SmartNoteSwitcher> ();
	}

	void FixedUpdate () 
	{
		if (switchScript.isSwitcherActivated == true) 
		{
			isPhraseSwitchCompleted = false;
			PhraseSelector ();
			isPhraseSwitchCompleted = true;//tells SmartNoteSwitcher Script to return isSwitchActivated to false
			Debug.Log ("Phrase selected at " + Time.time + " seconds!");
		}
	}

	void PhraseSelector()
	{
		if (switchScript.isAMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (switchScript.isAbMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (switchScript.isBMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (switchScript.isBbMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (switchScript.isCMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (switchScript.isDbMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (switchScript.isDMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (switchScript.isEbMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (switchScript.isEMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (switchScript.isFMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				} 
				else if (switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if(switchScript._midOctave == true) 
				{
				} 
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (switchScript.isFSharpMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (switchScript.isGMajor == true) 
		{
			if (switchScript.IChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor switchScript.IChord, No Music Selected!");
				}
			}
			else if (switchScript.iiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor switchScript.iiChord, No Music Selected!");
				}
			}
			else if (switchScript.iiiChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor switchScript.iiiChord, No Music Selected!");
				}
			}
			else if (switchScript.IVChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor switchScript.IVChord, No Music Selected!");
				}
			}
			else if (switchScript.VChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor switchScript.VChord, No Music Selected!");
				}
			}
			else if (switchScript.viChord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor switchScript.viChord, No Music Selected!");
				}
			}
			else if (switchScript.viAd2Chord == true) 
			{
				if (switchScript._lowOctave == true) 
				{
				} 
				else if (switchScript._midOctave == true) 
				{
				}
				else if(switchScript._hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor switchScript.viAd2Chord, No Music Selected!");
				}
			}
		} 	
	}
}
