﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

public class FMODCallbacks : MonoBehaviour 
{

	int currentMusicBar = 0;
	string lastMarker = "";
	FMOD.Studio.EVENT_CALLBACK beatCallback;
	FMOD.RESULT BeatEventCallback(FMOD.Studio.EVENT_CALLBACK_TYPE type, IntPtr instancePtr, IntPtr parameterPtr)
	{
		var instance = new FMOD.Studio.EventInstance(instancePtr);
		switch (type)
		{
		case FMOD.Studio.EVENT_CALLBACK_TYPE.TIMELINE_BEAT:
			{
				var parameter = (FMOD.Studio.TIMELINE_BEAT_PROPERTIES)Marshal.PtrToStructure(parameterPtr, typeof(FMOD.Studio.TIMELINE_BEAT_PROPERTIES));
				currentMusicBar = parameter.bar;
			}
			break;
		case FMOD.Studio.EVENT_CALLBACK_TYPE.TIMELINE_MARKER:
			{
				var parameter = (FMOD.Studio.TIMELINE_MARKER_PROPERTIES)Marshal.PtrToStructure(parameterPtr, typeof(FMOD.Studio.TIMELINE_MARKER_PROPERTIES));
				lastMarker = Marshal.PtrToStringAuto (parameter.name);
			}
			break;
		}
		return FMOD.RESULT.OK;
	}

	void PlayMusic()
	{
		beatCallback = new FMOD.Studio.EVENT_CALLBACK(BeatEventCallback);
		var musicInstance = FMODUnity.RuntimeManager.CreateInstance("event:/music/music");
		musicInstance.setCallback(beatCallback);
		musicInstance.start();
	}


}
