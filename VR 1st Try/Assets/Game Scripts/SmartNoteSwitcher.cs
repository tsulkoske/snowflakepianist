﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Runtime.InteropServices;

using Random = System.Random;

public class SmartNoteSwitcher : MonoBehaviour 

{
	//Octave Booleans, to monitor which octave of a chord is active
	public bool _lowOctave = false;
	public bool _midOctave = false;
	public bool _hiOctave = false;

	//Key Booleans, to monitor which key signature the piano is playing in
	public bool isAMajor, isAbMajor, isBMajor, isBbMajor, isCMajor, isDbMajor, isDMajor, isEbMajor, isEMajor, isFMajor, isFSharpMajor, isGMajor;

	//Chord Booleans, to monitor which Chord is being played
	public bool IChord, iiChord, iiiChord, IVChord, VChord, viChord, viAd2Chord;

//	//Wwise Callback Boolean, to see if Wwise is sending Callback Strings from the MIDI files
//	private bool WwiseCallback = false;

	//Detects whether there is a change in the Switch booleans
	//and is used by other snowflake scripts to trigger their own Selector functions.
	public bool isSwitcherActivated = false;
	private bool isJustStarted = false;

	//For receiving booleans from other scripts
	public GameObject DoubleStopSystem;
	public SmartDoubleStopSwitcher doubleStopScript;
	public GameObject PhraseSystem;
	public SmartPhraseSwitcher phraseScript;

	//When WwiseCallback == true, RandomOctaveSwitcher() is called in Update(), 
	//then according to the above bools, TenSidedDie is rolled and will decide if 
	//the piano switches to the next closest octave.
	Random randomOctaveSwitchPercent = new Random ();
	int TenSidedDie;

	//Random ints used to select the starting Wwise Event
	Random randomSelectKey = new Random ();
	Random randomSelectChord = new Random ();
	Random randomSelectOctave = new Random ();
	int keyInt;
	int chordInt;
	int octaveInt;

	//The number of collisions (notes played) before Wwise switches to another Chord within the Key
	Random random_NumberOfNotesInChord = new Random ();
	int numberOfNotesInt;
	int collisions_SinceLastChordSwitch;

	//Assigns a percentage that determines how likely it is that a given chord will play next
	Random random_ChordSwitchPercent = new Random ();
	int OneHundredSidedDie;

	[FMODUnity.EventRef]
	private string AMaj7SingleNotesLow = "event:/AMajor/AMaj7SingleNotesLow";

	[FMODUnity.EventRef]
	FMOD.Studio.EventInstance OctaveEv;
	FMOD.Studio.ParameterInstance NoteNumbersParam;

	//Choosing a random note in FMOD
	Random random_FMODNoteNumber = new Random ();
	int NoteNumberDie;
	int PreviousNoteNumber;

	int minNote;
	int maxNote;

	int[] switchNoteArray = new int[] { 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17 };
//	bool willSwitchNote = switchNoteArray.Contains (NoteNumberDie);



	void Start () 
	{
		//Reference the SmartDoubleStopSwitcher script for booleans
		DoubleStopSystem = GameObject.FindGameObjectWithTag ("Double_Stop_System");
		doubleStopScript = DoubleStopSystem.GetComponent<SmartDoubleStopSwitcher> ();

		//Reference the SmartPhraseSwitcher script for booleans
		PhraseSystem = GameObject.FindGameObjectWithTag ("Phrase_System");
		phraseScript = PhraseSystem.GetComponent<SmartPhraseSwitcher> ();

		numberOfNotesInt = random_NumberOfNotesInChord.Next (4, 10);
		collisions_SinceLastChordSwitch = 0;

		TenSidedDie = 0;
		OneHundredSidedDie = 0;

		keyInt = 1; /*randomSelectKey.Next (1, 12);*/
		chordInt = 1; /*randomSelectChord.Next (1, 7);*/
		octaveInt = 2; /*randomSelectOctave.Next (1, 3);*/

		switch (keyInt)
		{
			case 1: isAMajor = true; break;
			case 2: isAbMajor = true; break;
			case 3: isBMajor = true; break;
			case 4: isBbMajor = true; break;
			case 5: isCMajor = true; break;
			case 6: isDbMajor = true; break;
			case 7: isDMajor = true; break;
			case 8: isEbMajor = true; break;
			case 9: isEMajor = true; break;
			case 10: isFMajor = true; break;
			case 11: isFSharpMajor = true; break;
			case 12: isGMajor = true; break;

		}

		switch (chordInt)
		{
			case 1: IChord = true; break;
			case 2: iiChord = true; break;
			case 3: iiiChord = true; break;
			case 4: IVChord = true; break;
			case 5: VChord = true; break;
			case 6: viChord = true; break;
			case 7: viAd2Chord = true; break;
		}

		switch (octaveInt) 
		{
			case 1: _lowOctave = true; break;
			case 2: _midOctave = true; break;
			case 3: _hiOctave = true; break;
		}

		ChordSelector (); 

		isSwitcherActivated = true; //Activate the Selector functions of the other scripts
		isJustStarted = true;

		OctaveEv = FMODUnity.RuntimeManager.CreateInstance ("event:/AMajor/OctaveASwitch");
		OctaveEv.getParameter ("NoteNumbers", out NoteNumbersParam);

		NoteNumberDie = 0;

		OctaveEv.start ();
	}
		


	void OnParticleCollision (GameObject other)
	{
		

		HandModel handModel = null;
		if (other.transform.parent) 
		{
			handModel = other.transform.parent.GetComponent<HandModel> ();

			if (handModel == null) 
			{
				handModel = other.transform.parent.parent.GetComponent<HandModel> ();
			}
		}

		if (handModel == null)
			return;

//		collisions_SinceLastChordSwitch += 1; //adds 1 to the number of detected collisions (played notes) for current chord
//		Debug.Log (collisions_SinceLastChordSwitch + " total collisions.");


		NoteNumberDie = random_FMODNoteNumber.Next (minNote, maxNote); //Chooses the note to be played

		while (PreviousNoteNumber == NoteNumberDie) //Ensures notes are not played back-to-back 
		{
			NoteNumberDie = random_FMODNoteNumber.Next (minNote, maxNote);
		}

			
		NoteNumbersParam.setValue (NoteNumberDie); //Play event

		if (switchNoteArray.Contains (NoteNumberDie)) //Checks to see if the Note is close to another octave
		{
			RandomOctaveSwitcher ();
		}
		Debug.Log (NoteNumberDie);

		PreviousNoteNumber = NoteNumberDie;
	
		Debug.Log ("Triggered!");
	}




	void FixedUpdate () 
	{
		//Sets the Phrase and Double Stop particle systems to match the beginning note selection of the Single Note System
		if (isJustStarted == true) 
		{
			while (isSwitcherActivated == true) 
			{
				if (doubleStopScript.isDoubleStopSwitchCompleted == true && phraseScript.isPhraseSwitchCompleted == true) 
				{
					isSwitcherActivated = false;
					isJustStarted = false;
				}
			}
		}

		//Checks to see if the number of collisions matches the number of notes set for the current chord
		if (collisions_SinceLastChordSwitch >= numberOfNotesInt) 
		{
			ChordSwitcher ();//Switches chord, then activates ChordSelector()
			isSwitcherActivated = true; //activates the equivalent of ChordSelector() in other classes

			while (isSwitcherActivated == true) 
			{
				if (doubleStopScript.isDoubleStopSwitchCompleted == true && phraseScript.isPhraseSwitchCompleted == true) 
				{
					isSwitcherActivated = false;
				}
			}

			OneHundredSidedDie = 0;//Resets randomized Int to 0
			collisions_SinceLastChordSwitch = 0;//Resets detected Collisions to 0
			numberOfNotesInt = random_NumberOfNotesInChord.Next (4, 10);//Chooses a new random number of notes 
																	    //to be played for newly selected chord.
		}
	}




	//
	//
	//                !!*!!~-------------- CUSTOM FUNCTIONS: --------------~!!*!!
	//
	// 
	// 
	//
	// 

	//                  1. OCTAVE SWITCHER


	//	Smartly switches between octaves when playing through notes in a chord
	void RandomOctaveSwitcher()
	{
		if (TenSidedDie == 0)
		{
			TenSidedDie = randomOctaveSwitchPercent.Next (1, 10);
		}

		//LOW OCTAVE SWITCHER
		if (_lowOctave == true) 
		{
			//Reads the int from current octave and determines which Probability is triggered
			if (NoteNumberDie == 7) 
			{ 
				Debug.Log ("SwtichUpToMidHIGHProbability read!");
				if (TenSidedDie <= 8) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			} else if (NoteNumberDie == 6) 
			{	
				Debug.Log ("SwtichUpToMidMEDIUMProbability read!");
				if (TenSidedDie <= 5) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			} else if (NoteNumberDie == 5) 
			{	
				Debug.Log ("SwtichUpToMidLOWProbability read!");
				if (TenSidedDie <= 3) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
				return;
			} else
			Debug.Log ("No strings read from Wwise callback!");
		} 

		//MIDDLE OCTAVE SWITCHER
		else if (_midOctave == true) 
		{

			//Switch from Mid octave to High octave
			if (NoteNumberDie == 14) 
			{
				//Switch up to high, high probability
				if (TenSidedDie <= 8) {
					_midOctave = false;
					_lowOctave = false;
					_hiOctave = true;
					ChordSelector ();
				} else
					return;
			}
			else if (NoteNumberDie == 13) 
			{
				//Switch up to high medium probability
				if (TenSidedDie <= 5) {
					_midOctave = false;
					_lowOctave = false;
					_hiOctave = true;
					ChordSelector ();
				} else
					return;
			}
			else if (NoteNumberDie == 12) 
			{
				//Switch up to high low probability
				if (TenSidedDie <= 3) {
					_midOctave = false;
					_lowOctave = false;
					_hiOctave = true;
					ChordSelector ();
				} else
					return;
			}

			//Switch from Mid octave to Low octave
			else if (NoteNumberDie == 8) 
			{
				//Switch down to low high prob
				if (TenSidedDie <= 8) {
					_midOctave = false;
					_lowOctave = true;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (NoteNumberDie == 9) 
			{
				//Switch down to low medium prob
				if (TenSidedDie <= 5) {
					_midOctave = false;
					_lowOctave = true;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (NoteNumberDie == 10) 
			{
				//Switch down to lo low prob
				if (TenSidedDie <= 3) {
					_midOctave = false;
					_lowOctave = true;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else
			Debug.Log ("No strings read from Wwise callback!");
		} 

		//HIGH OCTAVE SWITCHER
		else if (_hiOctave == true) 
		{

			if (NoteNumberDie == 15) 
			{
				//Switch down to mid high prob
				if (TenSidedDie <= 8) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (NoteNumberDie == 16) 
			{
				//Switch down to mid medium prob
				if (TenSidedDie <= 5) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else if (NoteNumberDie == 17) 
			{
				//Switch down to mid medium prob
				if (TenSidedDie <= 3) {
					_midOctave = true;
					_lowOctave = false;
					_hiOctave = false;
					ChordSelector ();
				} else
					return;
			}
			else
			Debug.Log ("No strings read from Wwise callback!");

		}
		else
			return;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////



	//         2. CHORD SWITCHER



	//After a given number of collisions (notes played), ChordSwitcher() is called
	//to smartly alter the chord booleans and choose the next chord.
	void ChordSwitcher()
	{
		if (OneHundredSidedDie == 0) 
		{
			OneHundredSidedDie = random_ChordSwitchPercent.Next (1, 100);
		}

		//Switching from the I Chord
		if (IChord == true) 
		{
			//30% chance to switch to V
			if (OneHundredSidedDie <= 30)
			{
				VChord = true;
				IChord = false;
				ChordSelector ();
			}

			//24% chance to switch to IV
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				IVChord = true;
				IChord = false;
				ChordSelector ();
			}

			//13% chance to switch to iii
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				iiiChord = true;
				IChord = false;
				ChordSelector ();
			}

			//19% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 70 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 70 && OneHundredSidedDie <= 79) 
				{
					viChord = true;
					IChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					IChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to ii
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				iiChord = true;
				IChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the ii Chord
		else if (iiChord == true) 
		{
			//30% chance to switch to V
			if (OneHundredSidedDie <= 30)
			{
				VChord = true;
				iiChord = false;
				ChordSelector ();
			}

			//24% chance to switch to iii
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				iiiChord = true;
				iiChord = false;
				ChordSelector ();
			}

			//13% chance to switch to I
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				IChord = true;
				iiChord = false;
				ChordSelector ();
			}

			//19% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 70 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 70 && OneHundredSidedDie <= 79) 
				{
					viChord = true;
					iiChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					iiChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to IV
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				IVChord = true;
				iiChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the iii Chord
		else if (iiiChord == true) 
		{
			//30% chance to switch to IV
			if (OneHundredSidedDie <= 30)
			{
				IVChord = true;
				iiiChord = false;
				ChordSelector ();
			}

			//24% chance to switch to V
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				VChord = true;
				iiiChord = false;
				ChordSelector ();
			}

			//13% chance to switch to ii
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				iiChord = true;
				iiiChord = false;
				ChordSelector ();
			}

			//24% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 77) 
				{
					viChord = true;
					iiiChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					iiiChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to I
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				IChord = true;
				iiiChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the IV Chord
		else if (IVChord == true) 
		{
			//30% chance to switch to I
			if (OneHundredSidedDie <= 30)
			{
				IChord = true;
				IVChord = false;
				ChordSelector ();
			}

			//24% chance to switch to V
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				VChord = true;
				IVChord = false;
				ChordSelector ();
			}

			//13% chance to switch to ii
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				iiChord = true;
				IVChord = false;
				ChordSelector ();
			}

			//24% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 77) 
				{
					viChord = true;
					IVChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					IVChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to iii
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				iiiChord = true;
				IVChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the V Chord
		else if (VChord == true) 
		{
			//40% chance to switch to I
			if (OneHundredSidedDie <= 40)
			{
				IChord = true;
				VChord = false;
				ChordSelector ();
			}

			//23% chance to switch to IV
			else if (OneHundredSidedDie >= 41 && OneHundredSidedDie <= 64)
			{
				IVChord = true;
				VChord = false;
				ChordSelector ();
			}

			//4% chance to switch to ii
			else if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 69)
			{
				iiChord = true;
				VChord = false;
				ChordSelector ();
			}

			//39% chance to switch to vi or viAd2
			else if (OneHundredSidedDie >= 60 && OneHundredSidedDie <= 89) 
			{
				if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 77) 
				{
					viChord = true;
					VChord = false;
					ChordSelector ();
				}
				else
				{
					viAd2Chord = true;
					VChord = false;
					ChordSelector ();
				}
			}

			//10% chance to switch to iii
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				iiiChord = true;
				VChord = false;
				ChordSelector ();
			}
		} 

		//Switching from the vi Chord
		else if (viChord == true || viAd2Chord == true) 
		{
			//30% chance to switch to V
			if (OneHundredSidedDie <= 30)
			{
				VChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector ();
			}

			//24% chance to switch to IV
			else if (OneHundredSidedDie >= 31 && OneHundredSidedDie <= 55)
			{
				IVChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector ();
			}

			//13% chance to switch to ii
			else if (OneHundredSidedDie >= 56 && OneHundredSidedDie <= 69)
			{
				iiChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector ();
			}

			//24% chance to switch to I
			else if (OneHundredSidedDie >= 65 && OneHundredSidedDie <= 89) 
			{
				IChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector ();
			}

			//10% chance to switch to iii
			else if (OneHundredSidedDie >= 90 && OneHundredSidedDie <= 100) 
			{
				iiiChord = true;
				viChord = false;
				viAd2Chord = false;
				ChordSelector ();
			}
		} 
		else
			return;

	}

	////////////////////////////////////////////////////////////////////////////////////////


	//       3. CHORD SELECTOR


	//Decides which Wwise Event is called, 
	//which in turn decides which collection of notes to play.
	void ChordSelector()
	{
		Debug.Log ("Chord Selector Activated");

		if (isAMajor == true)  ////////     [--------- A MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
					minNote = 1;
					maxNote = 8;
					Debug.Log ("AMaj7SingleNotes LOW selected! " + "minNote = " + minNote + " and maxNote = " +maxNote);
				} 
				else if (_midOctave == true) 
				{
					minNote = 8;
					maxNote = 15;
					Debug.Log ("AMaj7SingleNotes MID selected!" + "minNote = " + minNote + " and maxNote = " +maxNote);
				} 
				else if (_hiOctave == true) 
				{
					minNote = 15;
					maxNote = 21;
					Debug.Log ("AMaj7SingleNotes HIGH selected!" + "minNote = " + minNote + " and maxNote = " +maxNote);
				} 
				else 
				{
					Debug.Log ("isAMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
					 
					Debug.Log ("Bmin7SingleNotesLow selected!");
				} 
				else if(_midOctave == true) 
				{
					
					Debug.Log ("Bmin7SingleNotesMid selected!");
				} 
				else if(_hiOctave == true) 
				{
					 
					Debug.Log ("Bmin7SingleNotesHigh selected!");
				} 
				else 
				{
					Debug.Log ("isAMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
					
					Debug.Log ("CSharpmin7SingleNotesLow selected!");
				} 
				else if(_midOctave == true) 
				{
					Debug.Log ("CSharpmin7SingleNotesMid selected!");
				} 
				else if(_hiOctave == true) 
				{
					
					Debug.Log ("CSharpmin7SingleNotesHigh selected!");
				} 
				else 
				{
					Debug.Log ("isAMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
					
					Debug.Log ("DMaj7SingleNotesLow selected!");
				} 
				else if(_midOctave == true) 
				{
					
					Debug.Log ("DMaj7SingleNotesMid selected!");

				} 
				else if(_hiOctave == true) 
				{
					
					Debug.Log ("DMaj7SingleNotesHigh selected!");
				} 
				else 
				{
					Debug.Log ("isAMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
					
					Debug.Log ("E7SingleNotesLow selected!");
				} 
				else if(_midOctave == true) 
				{
					
					Debug.Log ("E7SingleNotesMid selected!");
				} 
				else if(_hiOctave == true) 
				{
					
					Debug.Log ("E7SingleNotesHigh selected!");
				} 
				else 
				{
					Debug.Log ("isAMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
					
					Debug.Log ("FSharpmin7SingleNotesLow selected!");
				} 
				else if(_midOctave == true) 
				{
					
					Debug.Log ("FSharpmin7SingleNotesMid selected!");
				} 
				else if(_hiOctave == true) 
				{
					 
					Debug.Log ("FSharpmin7SingleNotesHigh selected!");
				} 
				else 
				{
					Debug.Log ("isAMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
					
					Debug.Log ("FSharpminAd2SingleNotesLow selected!");
				} 
				else if(_midOctave == true) 
				{
				
					Debug.Log ("FSharpminAd2SingleNotesMid selected!");
				} 
				else if(_hiOctave == true) 
				{
					
					Debug.Log ("FSharpminAd2SingleNotesHigh selected!");
				} 
				else 
				{
					Debug.Log ("isAMajor viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (isAbMajor == true) ////////     [--------- Ab MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isAbMajor viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (isBMajor == true) ////////     [--------- B MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBMajor viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (isBbMajor == true) ////////     [--------- Bb MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isBbMajor viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (isCMajor == true) ////////     [--------- C MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isCMajor viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (isDbMajor == true) ////////     [--------- Db MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDbMajor viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (isDMajor == true) ////////     [--------- D MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isDMajor viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (isEbMajor == true) ////////     [--------- Eb MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEbMajor viAd2Chord, No Music Selected!");
				}
			}
		} 
		else if (isEMajor == true) ////////     [--------- E MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isEMajor viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (isFMajor == true) ////////     [--------- F MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFMajor viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (isFSharpMajor == true) ////////     [--------- F# MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isFSharpMajor viAd2Chord, No Music Selected!");
				}
			}
		} 

		else if (isGMajor == true) ////////     [--------- G MAJOR --------]    //////////
		{
			if (IChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if (_midOctave == true) 
				{
				} 
				else if (_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor IChord, No Music Selected!");
				}
			}
			else if (iiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor iiChord, No Music Selected!");
				}
			}
			else if (iiiChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor iiiChord, No Music Selected!");
				}
			}
			else if (IVChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor IVChord, No Music Selected!");
				}
			}
			else if (VChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor VChord, No Music Selected!");
				}
			}
			else if (viChord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor viChord, No Music Selected!");
				}
			}
			else if (viAd2Chord == true) 
			{
				if (_lowOctave == true) 
				{
				} 
				else if(_midOctave == true) 
				{
				} 
				else if(_hiOctave == true) 
				{
				} 
				else 
				{
					Debug.Log ("isGMajor viAd2Chord, No Music Selected!");
				}
			}
		} 	
	}
}
